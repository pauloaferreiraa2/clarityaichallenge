This is my solution for the problem. However, I did not find the time to properly finish it and deploy it
in order to let you test it easily.

## Solution

My solution uses a HashMap in which the left host is the key to a list of objects, which contain the right host and timestamp.
Its O(1) complexity to get all the connections of the given host was a key factor for my decision. 

However, it is not the best solution to find connections between a time frame since all the elements need to be iterated.
A better solution could be, for example, use a sorted List and when getting the connections between `init_time` and `end_time`, start iterating from the connection closer to the `end_time`.

## First part of the problem

Given that all the logs are roughly sorted with a maximum of 5 minutes out of place. It means that given a `init_time` and `end_time`, 
the data between `init_time - (5*60)` and `end_time + (5*60)` is the only data that matters.
Therefore, my parser, only stores the data between that time frame, which allows to save a lot of memory. 
And the parser stops parsing when it finds the closest value to `end_time + (5*60)`, which allows to save memory and CPU usage.

## Second part of the problem

My idea was to have a task scheduled that is triggered once every hour. This is implemented on the `startUnlimitedInputParser`.
The ideal scenario would be to delete all the connections that have happened more than 1 hour ago, everytime this task runs.


## Architecture

The file `Connections` has the data and all the logic related to it.
The file `ConnectedHostsHandler` implements the solutions for both the first and second parts.

## Deployment

The idea was to deploy this application into docker and give a command for each different action. 
One to parse the file, another one to start the unlimited input parser and another one to run the unit tests.
Also, it would contain some environment variables for the configurable variables, such as:
- `GET_HOST_CONNECTIONS`
- `GET_HOST_RECEIVED_CONNECTIONS`
- `LOG_FILE_UNLIMITED_PARSER`

## Run
- ./gradlew :cleanTest :test --tests "ConnectionsTests" - To run unit tests
- ./gradlew -PmainClass=Main run - To run application

## Action Points
- Improve User UX
- Add input wrong format handlers
- Improve the way data is structured in order to optimize it data search.
- Deploy application
