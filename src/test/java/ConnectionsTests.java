import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConnectionsTests {
    private Connections connections;
    private String host;

    @Before
    public void init(){
        connections = Connections.getInstance();
        host = "fromHost1";

        connections.setConnection(host, "targetHost1", 123456L);
        connections.setConnection("fromHost2", "targetHost2", 123457L);
    }

    @After
    public void clean(){
        connections.clearConnections();
    }

    /**
     * Returns connections on both ways, if inside the given period
     */
    @Test
    public void connectedToHost_ReturnsHostConnections(){
        long initTime = 123452L;
        long endTime = 123459L;

        List<String> actualOutcome = connections.connectedToHost(host, initTime, endTime);

        //Check the the host has actual connections
        Assert.assertEquals(1, actualOutcome.size());

        for(String s:actualOutcome){
            Assert.assertEquals("targetHost1", s);
        }
    }

    /**
     * Returns empty host list, since period is not inside init_time and end_time
     */
    @Test
    public void connectedToHost_ReturnsEmptyHostList(){
        long initTime = 123452L;
        long endTime = 123453L;

        List<String> actualOutcome = connections.connectedToHost(host, initTime, endTime);

        //Check the the host has actual connections
        Assert.assertEquals(0, actualOutcome.size());

    }

    /**
     * This tests a setConnection when the fromHost does not exist
     */
    @Test
    public void setConnection_NewFromHost(){
        final String fromHost = "fromHost";
        final String targetHost = "targetHost";
        final long timestamp = 12345L;
        final ConnectionSettings cS = new ConnectionSettings(targetHost, timestamp);
        List<ConnectionSettings> connectionSettingsList = new ArrayList();
        connectionSettingsList.add(cS);

        connections.setConnection(fromHost, targetHost, timestamp);
        Map<String, List<ConnectionSettings>> actualOutcome = connections.getConnections();

        //Test size
        Assert.assertEquals(actualOutcome.size(), 3);

        //Check that 'targetHost' is equal to the added targetHost
        for(ConnectionSettings element:connections.getConnections().get(fromHost)){
            Assert.assertEquals(targetHost, element.getTargetHost());
        }
    }

    /**
     * This tests a setConnection when the fromHost exists
     */
    @Test
    public void setConnection_OldFromHost(){
        final String targetHost = "targetHost";
        final long timestamp = 12345L;
        final ConnectionSettings cS = new ConnectionSettings(targetHost, timestamp);
        List<ConnectionSettings> connectionSettingsList = new ArrayList();
        connectionSettingsList.add(cS);

        connections.setConnection(host, targetHost, timestamp);
        Map<String, List<ConnectionSettings>> actualOutcome = connections.getConnections();

        //Test size
        Assert.assertEquals(2, actualOutcome.size());

    }

    @Test
    public void clearConnections(){
        connections.clearConnections();

        Assert.assertEquals(0, connections.getConnections().size());
    }

    @Test
    public void hostMostGeneratedConnections_ReturnsHostWithMostGeneratedConnections(){
        connections.setConnection(host, "targetHost3", 123458L);

        Assert.assertEquals(host, connections.hostMostGeneratedConnections(123454L, 123459L));

    }

    /**
     * Given an init_time and end_time, returns a list of hosts between that period
     */
    @Test
    public void getConnectedFromHost_ReturnsHostsConnectedToHostBetweenPeriod(){
        long init_time = 123456L;
        long end_time = 123457L;

        for(String s:connections.getConnectedFromHost(host,init_time,end_time)){
            Assert.assertEquals("targetHost1", s);
        }
    }

    /**
     * Given an init_time and end_time, returns an empty list since there is no connections between that period
     */
    @Test
    public void getConnectedFromHost_OutsidePeriod_ReturnsEmptyList(){
        long init_time = 123452L;
        long end_time = 123453L;

        Assert.assertEquals(0, connections.getConnectedFromHost(host, init_time, end_time).size());
    }
}
