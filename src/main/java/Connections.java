import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Connections {
    private Map<String, List<ConnectionSettings>> connections;
    private static final Connections instance = new Connections();

    private Connections(){
        connections = new HashMap();
    }

    //Singleton Pattern
    public static Connections getInstance(){
        return instance;
    }

    /**
     * This method finds all the hosts that received a connection from a given host between a time period, ie, on a connection
     * @param fromHost Host that is the origin of the connection
     * @param initTime Get connections that happened after this timestamp
     * @param endTime Get connections that happened before this timestamp
     * @return List of hosts that received a connection from a given host
     *
     */
    public List<String> getConnectedFromHost(String fromHost, long initTime, long endTime){
        List<String> connectionsList = new ArrayList();

        try{
            for(ConnectionSettings cS : connections.get(fromHost)){
                if(initTime <= cS.getTimestamp() && cS.getTimestamp() < endTime)
                    connectionsList.add(cS.getTargetHost());
            }
        }finally {
            return connectionsList;
        }
    }

    /**
     * This method finds all the hosts connected to a given host between a time period. Given hostB, if there is a connection
     * hostA - hostB or hostB - hostA, it is considered that hostA is connected to hostB
     * @param host
     * @return
     */
    public List<String> connectedToHost(String host, long initTime, long endTime){
        List<String> connectionsList = new ArrayList();

        for(Map.Entry<String, List<ConnectionSettings>> entry: connections.entrySet()){
            for(ConnectionSettings c: entry.getValue()){
                if(host.equals(c.getTargetHost()) &&  (initTime <= c.getTimestamp() && c.getTimestamp() < endTime)){
                    connectionsList.add(entry.getKey());
                }
            }
        }

        List<ConnectionSettings> connectedToList = connections.get(host);
        if(connectedToList != null){
            for(ConnectionSettings c: connectedToList){
                if((initTime <= c.getTimestamp() && c.getTimestamp() < endTime))
                    connectionsList.add(c.getTargetHost());
            }
        }

        return connectionsList;
    }


    /**
     *
     * @param fromHost Origin of the connection
     * @param targetHost Target of the connection
     * @param timestamp When the connection happened
     */
    public void setConnection(String fromHost, String targetHost, long timestamp){
        ConnectionSettings connection = new ConnectionSettings(targetHost, timestamp);
        List<ConnectionSettings> connectionsList = connections.get(fromHost);

        if(connectionsList != null && connectionsList.add(connection)){
            connections.put(fromHost, connectionsList);
        }else{
            connectionsList = new ArrayList();
            connectionsList.add(connection);
            connections.put(fromHost, connectionsList);
        }
    }

    public void clearConnections(){
        connections.clear();
    }

    /**
     * Determines which host generated most connections between given period
     */
    public String hostMostGeneratedConnections(long initTime, long endTime){
        String host = "";
        int nConnections = 0;

        for(Map.Entry<String, List<ConnectionSettings>> entry: connections.entrySet()){
            int hostGeneratedConnections = 0;
            for(ConnectionSettings c:entry.getValue()){
                if(initTime <= c.getTimestamp() && c.getTimestamp() < endTime){
                    hostGeneratedConnections++;
                }
            }
            if(hostGeneratedConnections > nConnections) {
                nConnections = hostGeneratedConnections;
                host = entry.getKey();
            }
        }

        System.out.println("The host with most generated connections is " + host + " with " + nConnections +
                " connections");

        return host;
    }

    public Map<String, List<ConnectionSettings>> getConnections(){
        return this.connections;
    }
}
