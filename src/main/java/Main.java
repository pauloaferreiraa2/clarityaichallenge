import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        ConnectedHostsJHandler connectedHosts = ConnectedHostsJHandler.getInstance();
        long init_time = 1565647204351L;
        long end_time = 1565733598341L;
        String host = "Dashyra";
        Scanner scanner = new Scanner(System.in);

        try{
            char s = '1';
            while(s != '0'){
                System.out.println("Choose an option");
                System.out.println("1. Parse written file");
                System.out.println("2. Start unlimited parser");
                System.out.println("0. Finish");
                s = scanner.nextLine().charAt(0);
                String input = "";
                if(s == '1'){
                    System.out.println("Insert a host, an init_time and an end_time");
                    input = scanner.nextLine();
                    String[] tokens = input.split(" ");
                    System.out.println(connectedHosts.parseConnectedToHostPeriod(tokens[0], Long.valueOf(tokens[1]), Long.valueOf(tokens[2])));
                }else {
                    if(s == '2'){
                        connectedHosts.startUnlimitedInputParser();
                    }
                }
            }
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
