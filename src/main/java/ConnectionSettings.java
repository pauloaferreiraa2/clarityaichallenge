public class ConnectionSettings {
    private String targetHost;
    private long timestamp;

    public String getTargetHost() {
        return targetHost;
    }

    public void setTargetHost(String targetHost) {
        this.targetHost = targetHost;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public ConnectionSettings(String targetHost, long timestamp) {
        this.targetHost = targetHost;
        this.timestamp = timestamp;
    }
}
