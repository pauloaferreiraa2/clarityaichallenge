import javax.print.DocFlavor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class ConnectedHostsJHandler {

    private final String CONNECTIONS_FILE = "input-file-1000.txt";
    private final String CONNECTIONS_FILE_LIVE = "input-file-live.txt";
    private final String UNLIMITED_PARSER_FILE = "input-unlimited-file.txt";
    Logger log = Logger.getLogger(ConnectedHostsJHandler.class.getName());
    private final long fiveMinutes = 300;
    private String HOST = "Roddick";
    private Connections connections;
    private static final ConnectedHostsJHandler instance = new ConnectedHostsJHandler();

    private ConnectedHostsJHandler(){
        connections = Connections.getInstance();
    }

    //Singleton Pattern
    public static ConnectedHostsJHandler getInstance(){
        return instance;
    }

    public void parseFile(String file, Long initTime, Long endTime){
        try{
            FileReader input = new FileReader(file);
            BufferedReader bufRead = new BufferedReader(input);
            String myLine;

            while ((myLine = bufRead.readLine()) != null) {

                String[] tokens = myLine.split(" ");
                Long timestamp = Long.valueOf(tokens[0]);
                String fromHost = tokens[1];
                String targetHost = tokens[2];

                if(timestamp > endTime + fiveMinutes) break;

                if(!timestampBetween(initTime, endTime, timestamp) || timestamp < initTime - fiveMinutes) continue;

                connections.setConnection(fromHost, targetHost, timestamp);
            }
        }catch (Exception e){
            System.err.println("Error: " + e.getMessage());
        }
    }

    //Goal #1 - Parse the data with a time_init, time_end
    public List<String> parseConnectedToHostPeriod(String host, long initTime, long endTime){
        parseFile(CONNECTIONS_FILE, initTime, endTime);

        return connections.connectedToHost(host, initTime, endTime);
    }

    private Boolean timestampBetween(Long init_time, Long end_time, Long timestamp){
        return init_time <= timestamp && timestamp <= end_time;
    }

    public void liveParseFile(){
        System.out.println("Parsing file " + CONNECTIONS_FILE_LIVE + "...");
        try{
            FileReader input = new FileReader(CONNECTIONS_FILE_LIVE);
            BufferedReader bufRead = new BufferedReader(input);
            String myLine;

            while ((myLine = bufRead.readLine()) != null) {
                String[] tokens = myLine.split(" ");
                Long timestamp = Long.valueOf(tokens[0]);
                String fromHost = tokens[1];
                String targetHost = tokens[2];

                connections.setConnection(fromHost, targetHost, timestamp);
            }
        }catch (Exception e){
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void lastHourStatistics(){
        long timestampNow = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        long timestampHourAgo = timestampNow - 3600;
        System.out.flush();
        System.out.println("Last Hour statistics:");
        List<String> connectedToHost = connections.connectedToHost(HOST, timestampHourAgo, timestampNow);
        if(connectedToHost != null){
            System.out.println("Host '" +  HOST + "' during the last hour connected to:");
            for(String s:connectedToHost)
                System.out.println(s);
        }

        List<String> receivedConnections = connections.getConnectedFromHost(HOST, timestampHourAgo, timestampNow);
        if(connectedToHost != null){
            System.out.println("Host '" +  HOST + "' during the last hour connected to:");
            for(String s:connectedToHost)
                System.out.println(s);
        }
        connections.hostMostGeneratedConnections(timestampHourAgo, timestampNow);


    }

    //Goal #2 - Unilimited Input Parser
    public void startUnlimitedInputParser(){
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Starting unlimited input parser...");

        TimerTask hourlyTask = new TimerTask() {
            @Override
            public void run() {
                lastHourStatistics();
            }
        };
        scheduler.scheduleAtFixedRate(hourlyTask, 1, 1, TimeUnit.HOURS);

        try{
            char s = '1';
            while(s != '0'){
                System.out.println("Choose an option");
                System.out.println("1. Parse written file (configured)");
                System.out.println("2. Live file parsing");
                System.out.println("0. Finish unilimited parser");
                s = scanner.nextLine().charAt(0);
                if(s == '1'){
                    long timestampNow = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
                    long timestampHourAgo = timestampNow - 3600;
                    parseFile(UNLIMITED_PARSER_FILE, timestampHourAgo, timestampNow);
                    System.out.println(connections.getConnections());
                }else {
                    if(s == '2'){
                        Thread liveParser = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                liveParseFile();
                            }
                        });
                        liveParser.start();
                        System.out.println("Press 0 to exit");
                        if(scanner.nextLine().charAt(0) == '0') liveParser.join();
                    }
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            scheduler.shutdown();
        }
    }
}
